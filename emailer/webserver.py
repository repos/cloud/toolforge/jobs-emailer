# Copyright (C) 2021 Arturo Borrero Gonzalez <aborrero@wikimedia.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import logging
from typing import Literal
import fastapi
from prometheus_fastapi_instrumentator import Instrumentator
from pydantic import BaseModel
import uvicorn

LISTEN_TCP_PORT = 8081
LISTEN_ADDR = "0.0.0.0"


class HealthResponse(BaseModel):
    state: Literal["OK", "ERROR"]
    message: str


async def healthz() -> HealthResponse:
    """This function handles requests to /healthz (liveness probes by k8s)."""
    return HealthResponse(state="OK", message="Everything up and running")


async def webserver_run() -> None:
    """This class runs a small embedded webserver to handle some simple requests."""
    app = fastapi.FastAPI(debug=True)
    app.get("/healthz")(healthz)

    Instrumentator().instrument(app).expose(app)

    config = uvicorn.Config(
        app, port=LISTEN_TCP_PORT, host=LISTEN_ADDR, log_level=logging.DEBUG
    )
    server = uvicorn.Server(config=config)
    await server.serve()
