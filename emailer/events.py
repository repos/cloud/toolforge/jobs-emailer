# Copyright (C) 2022 Arturo Borrero Gonzalez <aborrero@wikimedia.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import logging
import json
from threading import Event, Lock
import time
from kubernetes import client, watch  # type: ignore
from dataclasses import dataclass, field
from enum import Enum, auto
from typing import Any


class JobEventNotRelevant(Exception):
    """Exception that indicates that a JobEvent is not relevant."""


class JobEventLabel(Exception):
    """Exception that indicates that there is a problem with pod labels."""


@dataclass(frozen=True)
class ExpectedLabel:
    """Class to represent an expected label in a pod object."""

    name: str
    required: bool
    values: list[str] | None = None


@dataclass(frozen=True)
class ExpectedLabels:
    """Class to store all labels we expect/understand from k8s."""

    labels: list[ExpectedLabel]

    def validate(self, pod_labels: dict[str, Any]) -> None:
        """Method to validate a set of labels."""
        for label in self.labels:
            pod_label = pod_labels.get(label.name, None)
            if label.required and pod_label is None:
                raise JobEventLabel(f"pod misses required label '{label.name}'")

            if label.values and pod_label not in label.values:
                raise JobEventLabel(
                    f"pod has wrong value in '{label.name}'. '{label.values}' vs '{pod_label}'"
                )


EXPECTED_LABELS = ExpectedLabels(
    labels=[
        ExpectedLabel(
            # we don't require this label, default is to don't send emails
            name="jobs.toolforge.org/emails",
            required=False,
        ),
        ExpectedLabel(
            # job type
            name="app.kubernetes.io/component",
            required=True,
            values=["jobs", "cronjobs", "deployments"],
        ),
        ExpectedLabel(
            # job name
            name="app.kubernetes.io/name",
            required=True,
        ),
        ExpectedLabel(
            # user name (tool account)
            name="app.kubernetes.io/created-by",
            required=True,
        ),
        ExpectedLabel(
            name="app.kubernetes.io/managed-by",
            required=True,
            values=["toolforge-jobs-framework"],
        ),
        ExpectedLabel(
            name="toolforge",
            required=True,
            values=["tool"],
        ),
    ]
)


class JobEmailsConfig(Enum):
    """Class to represent a Toolforge job email configuration."""

    NONE = auto()
    ONFAILURE = auto()
    ONFINISH = auto()
    ALL = auto()

    def __str__(self) -> str:
        """String representation."""
        return self.name.lower()

    @classmethod
    def from_event(self, event: dict[str, Any]) -> "JobEmailsConfig":
        """Returns a JobEmailsConfig from a k8s event dictionary."""
        jobemailsconfig = event["metadata"]["labels"].get(
            "jobs.toolforge.org/emails", "none"
        )
        if jobemailsconfig == "onfailure":
            return JobEmailsConfig.ONFAILURE
        if jobemailsconfig == "onfinish":
            return JobEmailsConfig.ONFINISH
        if jobemailsconfig == "all":
            return JobEmailsConfig.ALL

        return JobEmailsConfig.NONE


class JobType(Enum):
    """Class to represent a Toolforge job type."""

    UNKNOWN = auto()
    NORMAL = auto()
    CRONJOB = auto()
    CONTINUOUS = auto()

    def __str__(self) -> str:
        """String representation."""
        return self.name.lower()

    @classmethod
    def from_event(self, event: dict[str, Any]) -> "JobType":
        """Returns a JobType from a k8s event dictionary."""
        jobtype = event["metadata"]["labels"].get("app.kubernetes.io/component", "none")

        if jobtype == "jobs":
            return JobType.NORMAL
        elif jobtype == "cronjobs":
            return JobType.CRONJOB
        elif jobtype == "deployments":
            return JobType.CONTINUOUS

        return JobType.UNKNOWN


class ContainerState(Enum):
    """Class to represent a kubernetes container state."""

    UNKNOWN = auto()
    RUNNING = auto()
    TERMINATED = auto()
    WAITING = auto()

    def __str__(self) -> str:
        """String representation."""
        return self.name.lower()


@dataclass()
class JobEvent:
    """Class to represent a Toolforge Kubernetes job event."""

    podname: str | None
    phase: str | None = None
    container_state: ContainerState | None = ContainerState.UNKNOWN
    exit_code: int | None = None
    start_timestamp: str | None = field(compare=False, default=None)
    stop_timestamp: str | None = field(compare=False, default=None)
    reason: str | None = field(compare=False, default=None)
    message: str | None = field(compare=False, default=None)
    original_event: dict[str, Any] | None = field(compare=False, default=None)

    @classmethod
    def from_event(cls, event: dict[str, Any]) -> "JobEvent":
        """Builds a JobEvent object from a kubernetes event."""
        podname = event["metadata"]["name"]
        phase = event["status"]["phase"]

        # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1ContainerState.md
        statuses = event["status"].get("containerStatuses", None)
        if statuses is not None and statuses[0] is not None:
            containerstatus = statuses[0]
            running_state = containerstatus["state"].get("running", None)
            terminated_state = containerstatus["state"].get("terminated", None)
            waiting_state = containerstatus["state"].get("waiting", None)
        else:
            running_state = None
            terminated_state = None
            waiting_state = None

        if running_state is not None:
            start_timestamp = running_state.get("startedAt", None)
            container_state = ContainerState.RUNNING
            extra_args = dict(start_timestamp=start_timestamp)
        elif terminated_state is not None:
            start = terminated_state.get("startedAt", None)
            stop = terminated_state.get("finishedAt", None)
            exit_code = terminated_state.get("exitCode", None)
            reason = terminated_state.get("reason", None)
            message = terminated_state.get("message", None)
            container_state = ContainerState.TERMINATED
            extra_args = dict(
                start_timestamp=start,
                stop_timestamp=stop,
                exit_code=exit_code,
                reason=reason,
                message=message,
            )
        elif waiting_state is not None:
            reason = waiting_state.get("reason", None)
            message = waiting_state.get("message", None)
            container_state = ContainerState.WAITING
            extra_args = dict(
                reason=reason,
                message=message,
            )
        else:
            container_state = ContainerState.UNKNOWN
            extra_args = dict()

        common_args = dict(
            podname=podname,
            phase=phase,
            original_event=event,
            container_state=container_state,
        )
        args = {**common_args, **extra_args}

        return cls(**args)

    def __repr__(self) -> str:
        """String representation."""
        s = ""

        if self.podname:
            s += f"Pod '{self.podname}'. "

        if self.phase:
            s += f"Phase: '{self.phase.lower()}'. "

        if self.container_state:
            s += f"Container state: '{self.container_state}'. "

        if self.start_timestamp:
            s += f"Start timestamp {self.start_timestamp}. "

        if self.stop_timestamp:
            s += f"Finish timestamp {self.stop_timestamp}. "

        if self.exit_code:
            s += f"Exit code was '{self.exit_code}'. "

        if self.reason:
            s += f"With reason '{self.reason}'. "

        if self.message:
            s += f"With message: '{self.message}'. "

        return s


@dataclass
class Job:
    """Class to represent a collection of events related to a particular Toolforge tool."""

    name: str
    type: JobType
    emailsconfig: JobEmailsConfig | None = JobEmailsConfig.NONE
    events: list[JobEvent] = field(init=False, default_factory=list)

    def _relevance_test(self, new_event: JobEvent) -> None:
        """Evaluates if a new event is worth storing as job event."""
        if self.emailsconfig == JobEmailsConfig.NONE:
            raise JobEventNotRelevant(f"job emails config is {self.emailsconfig}")

        if new_event in self.events:
            raise JobEventNotRelevant("we already have a similar event (duplicated)")

        if (
            new_event.container_state == ContainerState.UNKNOWN
            and new_event.phase == "Pending"
        ):
            raise JobEventNotRelevant("this event has no meaningful information")

        if self.emailsconfig == JobEmailsConfig.ALL:
            logging.debug(
                f"user wants emails about all events on this job, caching event: {new_event}"
            )
            return

        if (
            self.emailsconfig == JobEmailsConfig.ONFINISH
            and new_event.container_state == ContainerState.TERMINATED
        ):
            logging.debug(
                f"user wants emails about onfinish events on this job, caching event: {new_event}"
            )
            return

        if (
            self.emailsconfig == JobEmailsConfig.ONFAILURE
            and new_event.container_state == ContainerState.TERMINATED
            and new_event.exit_code != 0
        ):
            logging.debug(
                f"user wants emails about onfailure events on this job, caching event: {new_event}"
            )
            return

        raise JobEventNotRelevant(
            f"found no reason to track this event: {self.name} {new_event}"
        )

    def add_event(self, event: dict[str, Any]) -> None:
        """Add an entry to the list of job events."""
        new_event = JobEvent.from_event(event)
        self._relevance_test(new_event)
        self.events.append(new_event)

    def __str__(self) -> str:
        """String representation."""
        return f"{self.name} ({self.type}) (emails: {self.emailsconfig})"


@dataclass
class UserJobs:
    """Class to represent all job events produced by an user."""

    username: str
    jobs: list[Job] = field(init=False, default_factory=list)

    def _get_or_create(self, event: dict[str, Any]) -> Job:
        """Get a previous cached job from this user or create a new one if none exists."""
        jobname = event["metadata"]["labels"]["app.kubernetes.io/name"]
        jobtype = JobType.from_event(event)
        jobemailsconfig = JobEmailsConfig.from_event(event)

        for job in self.jobs:
            if (
                job.name == jobname
                and job.type == jobtype
                and job.emailsconfig == jobemailsconfig
            ):
                return job

        new_job = Job(name=jobname, type=jobtype, emailsconfig=jobemailsconfig)
        return new_job

    def add_event(self, event: dict[str, Any]) -> None:
        """Add an event to the list of kubernetes job events."""
        job = self._get_or_create(event)
        job.add_event(event)

        # we just created this entry
        if job not in self.jobs:
            self.jobs.append(job)


@dataclass
class JobEvents:
    """Class to represent collected Toolforge Kubernetes job events."""

    cache: list[UserJobs] = field(init=False, default_factory=list)
    # needs to be thread safe as we run flush and add_event in different threads
    lock: Lock = field(init=False, default_factory=Lock)

    def _get_or_create(self, username: str) -> UserJobs:
        """Get the user job events object from the cache, or create one if it doesn't exists."""
        for userjobs in self.cache:
            if userjobs.username == username:
                return userjobs

        new_userjobs = UserJobs(username)
        return new_userjobs

    def add_event(self, event: dict[str, Any]) -> None:
        with self.lock:
            username = event["metadata"]["labels"]["app.kubernetes.io/created-by"]
            userjobs = self._get_or_create(username)
            userjobs.add_event(event)

            # we just created this entry
            if userjobs not in self.cache:
                self.cache.append(userjobs)

    def flush(self) -> None:
        with self.lock:
            self.cache.clear()
        logging.debug("cache flushed")


def event_early_filter(event: dict[str, Any], event_type: str) -> None:
    """Evaluate if a k8s pod event is interesting to the emailer, before any caching routine."""
    name = event["metadata"]["name"]
    namespace = event["metadata"]["namespace"]

    logging.debug(f"evaluating event relevance for pod '{namespace}/{name}'")

    if not namespace.startswith("tool-"):
        raise JobEventNotRelevant(f"not interested in namespace '{namespace}'")

    if event_type != "MODIFIED":
        raise JobEventNotRelevant(f"not interested in this type {event_type}")

    if event["metadata"].get("deletion_timestamp", None) is not None:
        raise JobEventNotRelevant("object being deleted")

    phase = event["status"]["phase"]
    # https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#pod-phase
    if phase not in ["Pending", "Running", "Succeeded", "Failed"]:
        raise JobEventNotRelevant(f"pod phase not relevant: {phase}")

    # ignore early some obvious discards by configuration
    # further filtering is done later when we decode and do the math to calculate if an
    # event matches the requested config
    labels = event["metadata"]["labels"]
    EXPECTED_LABELS.validate(labels)
    emails = labels.get("jobs.toolforge.org/emails", "none")
    if emails == "none":
        raise JobEventNotRelevant("user configuration requested no emails")

    logging.debug("event seems relevant in the early filter")


def task_watch_pods(job_events: JobEvents, stop_event: Event) -> None:
    corev1api = client.CoreV1Api()
    watcher = watch.Watch()

    # prepopulating resource version so the initial stream doesn't show us events since the
    # beginning of the history
    last_seen_version = corev1api.list_pod_for_all_namespaces(
        limit=1
    ).metadata.resource_version

    logging.debug("task_watch_pods() loop")

    for event in watcher.stream(
        corev1api.list_pod_for_all_namespaces,
        resource_version=last_seen_version,
    ):
        if stop_event.is_set():
            logging.info("Stopping on stop_event")
            return

        raw_event_dict = event["raw_object"]

        try:
            event_early_filter(raw_event_dict, event["type"])
            job_events.add_event(raw_event_dict)
        except KeyError as e:
            logging.error(f"potential bug while reading the k8s JSON, missing key {e}")
            logging.error("offending JSON follows:")
            logging.error(json.dumps(event, sort_keys=True, indent=4))
            pass
        except JobEventNotRelevant as e:
            logging.debug(f"ignoring job event: {e}")
            pass
        except JobEventLabel as e:
            logging.debug(f"ignoring job event: {e}")
            pass

        last_seen_version = event["object"].metadata.resource_version


def task_fake_watch_pods(job_events: JobEvents, stop_event: Event) -> None:
    logging.debug("loop")

    while not stop_event.is_set():
        fake_raw_event_dict = {
            "metadata": {
                "name": "fake_event_name",
                "namespace": "tool-fake-tool",
                "labels": {
                    "app.kubernetes.io/component": "deployments",
                    "jobs.toolforge.org/emails": "onfailure",
                    "app.kubernetes.io/created-by": "fake-tool",
                    "app.kubernetes.io/managed-by": "toolforge-jobs-framework",
                    "app.kubernetes.io/name": "fake-job",
                    "toolforge": "tool",
                },
            },
            "status": {
                "containerStatuses": [{"state": {"terminated": {}}}],
                "phase": "Failed",
            },
        }
        try:
            event_early_filter(fake_raw_event_dict, "MODIFIED")
            job_events.add_event(fake_raw_event_dict)
        except JobEventNotRelevant as e:
            logging.debug(f"ignoring job event: {e}")
            pass
        except JobEventLabel as e:
            logging.debug(f"ignoring job event: {e}")
            pass

        # non-async sleep to match the real one
        time.sleep(2)

    logging.info("Stopping on stop_event")
