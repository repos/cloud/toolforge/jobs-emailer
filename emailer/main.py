# Copyright (C) 2021 Arturo Borrero Gonzalez<aborrero@wikimedia.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from asyncio import (
    TaskGroup,
    to_thread,
    run,
)
import logging
from collections import deque
from threading import Event

from kubernetes import config  # type: ignore
from prometheus_client import Counter

from . import cfg, compose, events, send
from .events import JobEvents
from .webserver import webserver_run

EMAILS_SENT_METRIC = Counter(
    "jobs_emailer_emails_sent",
    "Number of emails sent",
    labelnames=["tool"],
)

# Flooding an email server is very easy, so a word on how this works to try avoiding such flood:
#  1) task_watch_pods(): watch pod events from kubernetes
#    * events are filtered out, we only care about certain events
#    * if a relevant event happens, we extract the info and cache them
#  2) task_compose_emails(): iterate the job event cache to compose actual emails and queue them
#  3) task_send_emails(): every Y seconds, send queued emails, up to a given max
#  4) we also read a configmap every X seconds, to allow reconfiguration without restarts
#     which should help reduce the amount of lost emails
#
# The ultimate goal is to collapse per-user events into a single email, and send emails in
# controlled-size batches to avoid flooding the email servers. Remember, there could be hundred of
# events happening at the same time.
# This means an user may get a single email with reports about several events that happened to
# several jobs.
#
# The emailq queue is just a normal FIFO queue.


async def _main() -> None:
    job_events = JobEvents()
    emailq: deque[compose.Email] = deque()
    # this is needed for the task running in a thread as cancel events are not thrown there
    stop_event = Event()

    # do a first pass to load the config specially to get the wait times for the environment we run in
    await cfg.task_read_configmap(do_loop=False)

    try:
        # Using the taskgroup as asyncio.gather does not properly cancel the other coroutines when one fails
        async with TaskGroup() as tg:
            tg.create_task(webserver_run())
            tg.create_task(compose.task_compose_emails(job_events, emailq))
            tg.create_task(send.task_send_emails(emailq, EMAILS_SENT_METRIC))
            if cfg.CFG_DICT["development"] == "yes":
                # for testing locally
                tg.create_task(
                    to_thread(events.task_fake_watch_pods, job_events, stop_event)
                )
            else:
                tg.create_task(cfg.task_read_configmap())
                tg.create_task(
                    to_thread(events.task_watch_pods, job_events, stop_event)
                )

    finally:
        logging.info("Sending stop event")
        stop_event.set()
        raise


def main() -> None:
    cfg.reconfigure_logging()
    logging.info("emailer starting!")

    if cfg.CFG_DICT["development"] != "yes":
        # TODO: proper auth
        config.load_incluster_config()

    run(_main())


if __name__ == "__main__":
    main()
