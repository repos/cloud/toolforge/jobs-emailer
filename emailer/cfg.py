# Copyright (C) 2021 Arturo Borrero Gonzalez <aborrero@wikimedia.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from asyncio import sleep, to_thread
import os
import sys
import logging
from typing import Any
from kubernetes import client  # type: ignore

CONFIGMAP_NAME = "jobs-emailer-configmap"
CONFIGMAP_NS = "jobs-emailer"

# some defaults
CFG_DICT = {
    # process events to compose emails every this many seconds
    # if you pick a big enough number here chances are the system can collapse several emails for
    # a same user together. They will get the email delayed, but there will be fewer emails flying.
    # Also, a low value could result in duplicated emails about similar events.
    # The default here is 6.5 minutes
    "task_compose_emails_loop_sleep": "400",
    # send emails every this many seconds
    "task_send_emails_loop_sleep": "30",
    # every time we send emails, send this many at max
    "task_send_emails_max": "10",
    # how often to read our configmap for reconfiguration
    "task_read_configmap_sleep": "10",
    # the domain emails will go to
    "email_to_domain": "toolsbeta.wmflabs.org",
    # the prefix needed in the to address, before the tool name
    "email_to_prefix": "toolsbeta",
    # emails will come from this address
    "email_from_addr": "root@toolforge.org",
    # smtp server FQDN to use for outbout emails
    "smtp_server_fqdn": "mail.toolforge.org",
    # smtp server port to use for outbout emails
    "smtp_server_port": "465",
    # send emails for real? this should help you debug this stuff without harm
    "send_emails_for_real": "no",
    # print debug information into stdout
    "debug": "yes",
    # skip using k8s directly
    "development": "no",
}


def reconfigure_logging() -> None:
    logging_format = (
        "%(asctime)s %(levelname)s (%(filename)s:%(lineno)d %(funcName)s): %(message)s"
    )
    logging.basicConfig(
        format=logging_format, stream=sys.stdout, datefmt="%Y-%m-%d %H:%M:%S"
    )

    logging_level = logging.DEBUG
    if CFG_DICT["debug"] != "yes":
        logging_level = logging.INFO

    logging.getLogger().setLevel(logging_level)
    client.rest.logger.setLevel(logging_level)
    CFG_DICT["development"] = os.getenv("DEVELOPMENT", "no")
    if CFG_DICT["development"] == "yes":
        logging.warning("DEVELOPMENT MODE ENABLED, shortening the loops to 10s")
        CFG_DICT["task_compose_emails_loop_sleep"] = "10"
        CFG_DICT["task_read_configmap_sleep"] = "10"
        CFG_DICT["task_send_emails_loop_sleep"] = "10"


def reconfigure(configmap: Any) -> None:
    for key in configmap.data:
        if key not in CFG_DICT:
            logging.warning(
                f"ignoring unknown config key '{key}' (doesn't have a previous value)"
            )
            continue

        CFG_DICT[key] = configmap.data[key]

    reconfigure_logging()
    logging.info(f"new configuration: {CFG_DICT}")


async def task_read_configmap(do_loop: bool = True) -> None:
    corev1api = client.CoreV1Api()
    last_seen_version = 0
    # we want to spend 2 seconds at most reading the configmap
    _request_timeout = 2

    while True:
        logging.debug("task_read_configmap() starting loop")

        try:
            configmap = await to_thread(
                corev1api.read_namespaced_config_map,
                name=CONFIGMAP_NAME,
                namespace=CONFIGMAP_NS,
                _request_timeout=_request_timeout,
            )

        except client.exceptions.ApiException as e:
            logging.warning(
                f"unable to query configmap {CONFIGMAP_NS}/{CONFIGMAP_NAME}: "
                f"{e.status} {e.reason}. Will use previous config."
            )

        else:
            new_version = int(configmap.metadata.resource_version)
            if new_version > last_seen_version:
                last_seen_version = new_version
                reconfigure(configmap)

        if not do_loop:
            logging.debug("task_read_configmap() exiting (do_loop==False)")
            break

        sleep_interval = int(CFG_DICT["task_read_configmap_sleep"])
        logging.debug(f"task_read_configmap() sleeping {sleep_interval}")
        await sleep(sleep_interval)
