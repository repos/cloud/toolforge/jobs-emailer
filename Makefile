# this file is only useful in a local development environment, as the real-file
# build and deploy is done via gitlab-ci + toolforge-deploy repository

# not generating any local files
.PHONY: run image kind_load rollout build-and-deploy-local

# the first target is the one executed by default when called with just `make`
default: build-and-deploy-local

image:
	bash -c -- "source <(minikube docker-env) || : && docker build --target image -f .pipeline/blubber.yaml . -t jobs-emailer:dev"

kind_load:
	bash -c -- "hash kind 2>/dev/null && kind load docker-image jobs-emailer:dev --name toolforge || :"

rollout:
	bash -c -- "kubectl rollout restart -n jobs-emailer deployment jobs-emailer || :"

build-and-deploy-local: image kind_load rollout
	./deploy.sh
