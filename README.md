# Jobs emailer

This is the source code of the Toolforge Jobs emailer component.

It emails users when events about their jobs happen.

See also: https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Kubernetes/Jobs_framework

## Development
You can run on [lima kilo](https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo) or a minimal test using:
```bash
env DEVELOPMENT=yes poetry run python -m emailer
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
