from tests.context import JobEvents, JobEmailsConfig, Email, compose_email
from tests.fake_k8s import FakeK8sPodGenerator


def test_email_basic() -> None:
    """Basic test."""
    email = Email(
        subject="subject",
        to_addr="to@example.com",
        from_addr="from@example.com",
        body="body",
        tool="sometool",
    )
    assert email
    assert email.message()


def test_email_compose() -> None:
    """Basic compose test."""
    cache = JobEvents()

    cache.add_event(
        FakeK8sPodGenerator.new(phase="Running", job_emails=JobEmailsConfig.ALL)
    )
    assert len(cache.cache) == 1

    for userjobs in cache.cache:
        email = compose_email(userjobs)
        assert email
        assert email.message()
