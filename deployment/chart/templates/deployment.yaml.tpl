---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: jobs-emailer
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "jobs-emailer.labels" . | nindent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      name: jobs-emailer
      {{- include "jobs-emailer.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        name: jobs-emailer
        {{- include "jobs-emailer.selectorLabels" . | nindent 8 }}
    spec:
      serviceAccountName: jobs-emailer
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels:
              name: jobs-emailer
              {{- include "jobs-emailer.selectorLabels" . | nindent 14 }}
      containers:
      - name: jobs-emailer
        image: {{ .Values.image.name }}:{{ .Values.image.tag }}
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        ports:
          - name: webserver
            containerPort: 8081
        securityContext:
          runAsUser: 999
        livenessProbe:
          httpGet:
            path: /healthz
            port: webserver
          initialDelaySeconds: 10
          periodSeconds: 30
          # 28 seconds is periodSeconds (30) minus 2, meaning: timeout just before the next probe
          timeoutSeconds: 28
