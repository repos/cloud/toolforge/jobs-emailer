---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: jobs-emailer
  labels:
    {{- include "jobs-emailer.labels" . | nindent 4 }}
