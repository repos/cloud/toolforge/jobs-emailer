---
apiVersion: v1
kind: ConfigMap
metadata:
  name: jobs-emailer-configmap
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "jobs-emailer.labels" . | nindent 4 }}
data: {{ .Values.configmap_data | toYaml | nindent 2 }}
