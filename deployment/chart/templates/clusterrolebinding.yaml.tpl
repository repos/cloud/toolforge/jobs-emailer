---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: jobs-emailer
  labels:
    {{- include "jobs-emailer.labels" . | nindent 4 }}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: jobs-emailer
subjects:
- kind: ServiceAccount
  name: jobs-emailer
  namespace: {{ .Release.Namespace }}
