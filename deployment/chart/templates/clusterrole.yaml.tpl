---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: jobs-emailer
  labels:
    {{- include "jobs-emailer.labels" . | nindent 4 }}
rules:
- apiGroups:
  - policy
  resourceNames:
  - default
  resources:
  - podsecuritypolicies
  verbs:
  - use
- apiGroups:
  - ""
  resources:
  - pods
  - configmaps
  verbs:
  - watch
  - get
  - list
